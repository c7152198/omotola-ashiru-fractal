﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace C7152198FRACTAL
{
    public partial class Form1 : Form
    {
        private const  int MAX = 256;      // max iterations
        private const double SX = -2.025; // start value real
        private const double SY = -1.125; // start value imaginary
        private const double EX = 0.6;    // end value real
        private const double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle, finished;
        private static float xy;
        private System.Drawing.Bitmap picture; //picture for the fractal and area for the area box
        private System.Drawing.Bitmap picture2; //picture for the fractal and area for the area box
                                               // private Image picture;
        private Graphics g1,g2;
        private  HSB HSBcol;

      
        public Form1()
        {
            InitializeComponent();
            init();
            start();
            Refresh();
        }

      

        public void init() // all instances will be prepared
        {       
            finished = false;
            this.Cursor = System.Windows.Forms.Cursors.Cross;
            x1 = Width;
            y1 = Height;
            xy = (float)x1 / (float)y1;
            picture = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            picture2 = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            g1 = Graphics.FromImage(picture);
            g2 = Graphics.FromImage(picture2);
            pictureBox1.Image = picture;
            finished = true;
        }
        private void initvalues() // reset start values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        }
        public void start()
        {
            action = false;
            rectangle = false;
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot();

        }
       
        public void destroy() // delete all instances 
        {
            if (finished)
            { 
                picture = null;
                picture2 = null;
                g1 = null;
                g2 = null;
                GC.Collect();
             }
        }

    

        public void stop()
        {
        }

        public void update()
        {
               if (rectangle)
            {
                Pen whitePen = new Pen(Color.White);
                if (xs < xe)
                {
                    if (ys < ye) g2.DrawRectangle(whitePen,xs, ys, (xe - xs), (ye - ys));
                    else g2.DrawRectangle(whitePen,xs, ye, (xe - xs), (ys - ye));
                }
                else
                {
                    if (ys < ye) g2.DrawRectangle(whitePen,xe, ys, (xs - xe), (ye - ys));
                    else g2.DrawRectangle(whitePen,xe, ye, (xs - xe), (ys - ye));
                }
            }
        }
     

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
           // pictureBox1.Image = picture;
            e.Graphics.DrawImage(picture2,0,0);
            if (!rectangle)
            {
                e.Graphics.DrawImage(picture, 0, 0);

            }

        }

        private void mandelbrot() // calculate all points
        {
            int x, y;
            float h, b, alt = 0.0f;

            action = false;
            Pen MandelPen = new Pen(Color.White);
            this.Text =("Mandelbrot-Set will be produced - please wait...");
            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // color value
                    if (h != alt)
                    {
                        b = 1.0f - h * h;

                        HSBcol = new HSB(h * 255, 0.8f * 255, b * 255);
                        Color C = HSBcol.Color;
                        MandelPen = new Pen(C);

                        alt = h;

                    }          
                    g1.DrawLine(MandelPen, x, y, x + 1, y);
                }
          
            this.Text =("Mandelbrot-Set ready - please select zoom area with pressed mouse.");
            action = true;

        }

        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }

      



        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (action)
            {
                xs = e.X;
                ys = e.Y;
            }

        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            int z, w;

            
            if (action)
            {
                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                mandelbrot();
                rectangle = false;
                 Refresh();
            }


        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left) { 
            if (action)
            {
                xe = e.X;
                ye = e.Y;
                update();
                rectangle = true;
               Refresh();                //repaint();
            }
        }
            g2.Clear(Color.Transparent);

        }
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SavePicture();
        }
        public void SavePicture()
        {
            string fileName = "Bitmp3.bmp";
            SaveFileDialog savedlg = new SaveFileDialog();

            savedlg.InitialDirectory = Path.GetDirectoryName(fileName);
            savedlg.FileName = Path.GetFileNameWithoutExtension(fileName);
            savedlg.AddExtension = true;
            savedlg.Filter = "Windows Bitmap (*.bmp)|*.bmp";


            if (savedlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    picture.Save(savedlg.FileName);
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, Text);
                    return;
                }
                fileName = savedlg.FileName;
          MessageBox.Show( "Saved to " + Path.GetFileName(fileName));
            }

    }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";
                dlg.Filter = "bmp files (*.bmp)|*.bmp";
                pictureBox1.Image = null; 
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                   
                    picture = new Bitmap(Image.FromFile(dlg.FileName), pictureBox1.Width, pictureBox1.Height);
                    Refresh();
                    g1 = Graphics.FromImage(picture);
                    Refresh();
                    pictureBox1.Image = picture;
                    Refresh();
                    finished = true;
                    MessageBox.Show(dlg.FileName.ToString() + " opened");
                    action = true;

                }
                else
                {
                
                    MessageBox.Show("nothing opened");
                }
            }
            
            Refresh();

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public String getAppletInfo()
        {
            return "fractal.class - Mandelbrot Set a Java Applet by Eckhard Roessel 2000-2001";
        }
    }
}
